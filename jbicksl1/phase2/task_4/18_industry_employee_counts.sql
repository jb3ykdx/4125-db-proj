with indus_comps (ind_id, title, comp_id) as
	(select ind_id, title, comp_id
	 from gics natural join comp_sub_indus
	),
indus_jobs (ind_id, title, job_code) as
	(select ind_id, title, job_code
	 from indus_comps natural join job
	),
current_employees (ind_id, title, person_id) as
	(select ind_id, title, person_id
	 from indus_jobs natural join works
	 where current_date >= start_date and current_date <= end_date
	)
select ind_id, title, count (unique person_id)
from current_employees,
group by title, ind_id;
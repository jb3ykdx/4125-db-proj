with pos_jobs (job_code, title) as
	(select job_code, title
	 from position natural join job
	 where pos_code = &pos_code
	),
job_holders (person_id, title, start_year, end_year) as
	(select person_id, title, extract (year from start_date), extract (year from end_date)
	 from pos_jobs natural join works
	)
select person_id, name, title, start_year, end_year
from job_holders natural join person;
with numeric_skills(sk_level, lv_num) as
	(select sk_level,
		case
			where sk_level = 'beginner' then 0
			where sk_level = 'medium' then 1
			where sk_level = 'advanced' then 2
			else -1
		end
	 from requires),
present_skills(sk_code, present_num) as
	(select sk_code, lv_num
	 from has_skills natural join numeric_skills
	 where person_id = &person_id),
necessary_skills(sk_code, necessary_num) as
	(select sk_code, lv_num
	 from requires natural join numeric_skills
	 where pos_code = &pos_code),
absent_skills(sk_code, absent_num) as
	(select sk_code, necessary_num
	 from necessary_skills natural left outer join present_skills
	 where not (present_num >= necessary_num)),
numeric_covers(c_code, sk_code, covers_num) as
	(select c_code, sk_code, lv_num
	 from covers natural join numeric_skills)
unmet_covers(c_code) as
	(select unique(c_code)
	 from absent_skills natural left outer join numeric_covers
	 where not (covers_num >= absent_num))
select c_code, title
from course
where c_code not in unmet_covers;
with industry_jobs(job_code) as
	(select job_code
	 from job natural join company
	 where industry_group = &industry_group),
industry_works(person_id, job_code, start_date, end_date) as
	(select job_code, person_id
	 from works natural join industry_jobs),
most_recent_starts(person_id, start_date) as
	(select person_id, max(start_date)
	 from industry_works
	 group by person_id),
most_recent_ends(person_id, end_date) as
	(select person_id, max(end_date)
	 from industry_works
	 where end_date < current_date
	 group by person_id),
most_recent_changes(person_id, change_date) as
	(select person_id,
		case
			when start_date > end_date then start_date
			else end_date
		end
	 from most_recent_starts natural join most_recent_ends),
adjusted_job_pays(person_id, job_code, start_date, end_date, pay_rate) as
	(select person_id, job_code,
		case
			when pay_type = 'wage' then pay_rate * 1920
			else pay_rate
		end
	 from job natural join industry_works),
current_earnings(person_id, current_earnings) as
	(select person_id, sum(pay_rate)
	 from adjusted_job_pays
	 where start_date <= current_date
		and not(end_date < current_date)
	 group by person_id),
previous_earnings(person_id, previous_earnings) as
	(select person_id, sum(pay_rate)
	 from adjusted_job_pays natural join most_recent_changes
	 where start_date <= change_date
		and not(end_date < change_date)
	 group by person_id)
select count(person_id)
from current_earnings natural join previous_earnings
where current_earnings > previous_earnings;
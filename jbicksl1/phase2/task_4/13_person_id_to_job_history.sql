with job_history (pos_code, job_code, comp_id) as
	(select pos_code, job_code, comp_id
	 from works natural join job
	 where person_id = &person_id
	),
company_history (pos_code, job_code, company_name) as
	(select pos_code, job_code, name
	 from job_history natural join company
	)
select title, job_code
from company_history natural join position;
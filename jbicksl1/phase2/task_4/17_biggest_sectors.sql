with comp_jobs (comp_id, name, job_code) as
	(select comp_id, name, job_code
	 from company natural join job
	),
current_employees (comp_id, name, person_id) as
	(select comp_id, name, person_id
	 from comp_jobs natural join works
	 where current_date >= start_date and current_date <= end_date
	),
employee_counts (comp_id, name, employee_count) as
	(select comp_id, name, count (unique person_id)
	 from current_employees
	)
select comp_id, name, max (employee_count)
from employee_counts
group by name, comp_id;

with indus_comps (ind_id, title, comp_id) as
	(select ind_id, title, comp_id
	 from gics natural join comp_sub_indus
	),
indus_jobs (ind_id, title, job_code) as
	(select ind_id, title, job_code
	 from indus_comps natural join job
	),
current_employees (ind_id, title, person_id) as
	(select ind_id, title, person_id
	 from indus_jobs natural join works
	 where current_date >= start_date and current_date <= end_date
	),
employee_counts (ind_id, title, employee_count) as
	(select ind_id, title, count (unique person_id)
	 from current_employees,
	 group by ind_id, title
	)
select ind_id, title, max (employee_count)
from employee_counts
group by title, ind_id;

with grp_comps (ind_id, title, comp_id) as
	(select ind_id, title, comp_id
	 from gics natural join company
	 where industry_group = ind_id
	),
grp_jobs (ind_id, title, job_code) as
	(select ind_id, title, job_code
	 from grp_comps natural join job
	),
current_employees (ind_id, title, person_id) as
	(select ind_id, title, person_id
	 from grp_jobs natural join works
	 where current_date >= start_date and current_date <= end_date
	),
employee_counts (ind_id, title, employee_count) as
	(select ind_id, title, count (unique person_id)
	 from current_employees,
	 group by ind_id, title
	)
select ind_id, title, max (employee_count)
from employee_counts
group by title, ind_id;
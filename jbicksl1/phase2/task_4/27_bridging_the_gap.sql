with numeric_skills(sk_level, lv_num) as
	(select sk_level,
		case
			when sk_level = 'beginner' then 0
			when sk_level = 'medium' then 1
			when sk_level = 'advanced' then 2
			else -1
		end
	from requires),
pos_skills(sk_code, pos_num) as
	(select sk_code, lv_num
	 from requires natural join numeric_skills
	 where pos_code = &pos_code),
numeric_has_skill(person_id, sk_code, has_num) as
	(select person_id, sk_code, lv_num
	 from has_skill natural join numeric_skills
	 where sk_code in (select sk_code
			   from pos_skills)),
unmet_pos_skills(person_id, sk_code, unmet_num) as
	(select person_id, sk_code, pos_num
	 from pos_skills natural left outer join numeric_has_skill
	 where not (has_num >= pos_num)),
unmet_pos_skill_counts(person_id, skill_count) as
	(select person_id, count(sk_code)
	 from unmet_pos_skills
	 group by person_id),
k_list(person_id, sk_code, k_num) as
	(select person_id, sk_code, unmet_num
	 from unmet_pos_skills natural join unmet_pos_skill_counts
	 where skill_count < 4)
numeric_covers(c_code, sk_code, covers_num) as
	(select c_code, sk_code, lv_num
	 from covers natural join numeric_skills),
unmet_k_skills(c_code, person_id, sk_code) as
	(select c_code, person_id, sk_code
	 from k_list natural left outer join numeric_covers
	 where not (covers_num >= k_num)),
select person_id, c_code
from course
where (c_code, person_id) not in (select c_code, person_id
				  from unmet_k_skills)
with numeric_skills(sk_level, lv_num) as
	(select sk_level,
		case
			when sk_level = 'beginner' then lv_num = 0
			when sk_level = 'medium' then lv_num = 1
			when sk_level = 'advanced' then lv_num = 2
			else -1
		end
	 from requires),
available_skills(sk_code, available_num) as
	(select sk_code, lv_num
	 from has_skill natural join numeric_skills
	 where person_id = &person_id),
numeric_requires(pos_code, sk_code, required_num) as
	(select pos_code, sk_code, lv_num
	 from require natural join numeric_skills),
unmet_position_requirements(pos_code) as
	(select unique (pos_code)
	 from numeric_requires natural left outer join available_skills
	 where not (available_num >= required_num)),
qualified_positions(pos_code) as
	(select pos_code
	 from position
	 where pos_code not in unmet_position_requirements),
jobs_under_positions(job_code) as
	(select job_code
	 from job
	 where pos_code in qualified_positions),
numeric_req_by_job(job_code, sk_code, rbj_num) as
	(select job_code, sk_code, lv_num
	 from req_by_job natural join numeric_skills
	 where job_code in jobs_under_positions),
unmet_job_requirements(job_code) as
	(select unique (job_code)
	 from numeric_req_by_job natural left outer join available_skills
	 where not (available_num >= required_num)),
qualified_jobs(job_code) as
	(select job_code
	 from job
	 where job_code not in unmet_job_requirements),
rate_adjusted_jobs(job_code, pay_rate) as
	(select job_code,
		case
			when pay_type = 'wage' then pay_rate * 1920
			else pay_rate
		end
	 from job
	 where job_code in qualified_jobs)
select max(pay_rate)
from rate_adjusted_jobs;
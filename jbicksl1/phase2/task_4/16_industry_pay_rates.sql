with industry_level_gics (ind_id, title) as
	(select ind_id, title
	 from gics
	 where ind_level = 2
	),
industry_companies (ind_id, title, comp_id) as
	(select ind_id, title, comp_id
	 from industry_level_gics natural join comp_sub_indus
	),
industry_jobs (ind_id, title, pay_rate, pay_type) as
	(select ind_id, title, pay_rate, pay_type
	 from industry_companies natural join job
	),
industry_pay_rates (ind_id, title, pay_rate) as
	(select ind_id, title,
		case
			when pay_type = 'wage' then pay_rate * 1920
			else pay_rate
		end
	 from industry_jobs)
select ind_id, title, avg (pay_rate), min (pay_rate), max (pay_rate)
from industry_pay_rates
group by title, ind_id;
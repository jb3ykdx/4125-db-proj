with numeric_skills(sk_level, lv_num) as
	(select sk_level,
		case
			when sk_level = 'beginner' then 0
			when sk_level = 'medium' then 1
			when sk_level = 'advanced' then 2
			else -1
		end
	 from requires),
pos_skills(sk_code, pos_num) as
	(select sk_code, lv_num
	 from requires natural join numeric_skills
	 where pos_code = &pos_code),
numeric_has_skill(person_id, sk_code, has_num) as
	(select person_id, sk_code, lv_num
	 from has_skill natural join numeric_skills
	 where sk_code in (select sk_code
			   from pos_skills)),
unmet_pos_skills(person_id) as
	(select unique (person_id)
	 from pos_skills natural left outer join numeric_has_skill
	 where not (has_num >= pos_num))
select first_name, last_name, email
from person
where person_id not in unmet_pos_skills;
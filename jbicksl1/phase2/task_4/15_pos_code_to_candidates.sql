with previous_job_holders (person_id) as
	(select person_id, end_date
	 from works natural join job
	 where pos_code = &pos_code
	 	and end_date < current_date
	)
select person_id, first_name, last_name
from previous_job_holders natural join person;
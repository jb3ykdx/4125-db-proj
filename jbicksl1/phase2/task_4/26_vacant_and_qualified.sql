with filled_jobs(person_id, job_code) as
	(select job_code
	 from works
	 where start_date <= current_date
		and not(end_date < current_date)),
vacant_jobs(job_code, pos_code) as
	(select job_code, pos_code
	 from job
	 where job_code not in (select job_code
				from filled_jobs)),
unemployed_people(person_id) as
	(select person_id
	 from person
	 where person_id not in (select person_id
				 from filled_jobs)),
numeric_skills(sk_level, lv_num) as
	(select sk_level,
		case
			when sk_level = 'beginner' then 0
			when sk_level = 'medium' then 1
			when sk_level = 'advanced' then 2
			else -1
		end
	 from requires),
numeric_requires(pos_code, sk_code, requires_num) as
	(select pos_code, sk_code, lv_num
	 from requires natural join numeric_skills),
unemployed_has_skill(person_id, sk_code, sk_level) as
	(select person_id, sk_code, sk_level
	 from has_skill natural join unemployed_people),
numeric_has_skill(person_id, sk_code, has_num) as
	(select person_id, sk_code, lv_num
	 from unemployed_has_skill natural join numeric_skills),
unqualified_for_position(person_id, pos_code) as
	(select person_id, pos_code
	 from numeric_requires natural left outer join numeric_has_skill
	 where not(has_num >= requires_num)),
vacancies(pos_code, job_count) as
	(select pos_code, count(job_code)
	 from vacant_jobs
	 group by pos_code),
qualified_candidates(pos_code, person_id) as
	(select pos_code, person_id
	 from vacant_jobs, unemployed_people
	 where (pos_code, person_id) not in unqualified_for_position),
qualified_count(pos_code, person_count) as
	(select pos_code, count(person_id)
	 from qualified_candidates
	 group by pos_code),
fillable_vacancies(pos_code, vacancy_count) as
	(select pos_code, job_count - person_count
	 from vacancies natural join qualified_count)
select pos_code, max(vacancy_count)
from fillable_vacancies
group by pos_code;
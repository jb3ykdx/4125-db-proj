-- dependency level I

-- course


-- person
insert into person values(10000000,'Axton','Jones','male',86941,'Carnation Ln','Shreveport','Louisiana',71037,'US');
insert into person values(10000001,'Olivia','Sanders','female',7948,'Fulton St','Baton Rouge','Louisiana',70791,'US');
insert into person values(10000002,'Mariana','Thurber','female',79615,'Louis Prima Dr W','Crowley','Louisiana',70526,'US');
insert into person values(10000003,'Lorenzo','Dominguez','male',16454,'33rd St','Alexandria','Louisiana',71303,'US');
insert into person values(10000004,'Piper','Coble','female',87443,'Charmes Ct','Benton','Louisiana',71006,'US');

-- dependency level II

-- company
-- covers
-- has_skill
-- phone
-- pre_req
-- section

-- dependency level III

-- comp_sub_indus
-- job
-- offered_by
-- takes

-- dependency level IV

-- req_by_job
-- works
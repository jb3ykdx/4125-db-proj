-- dependency level IV

delete from req_by_job;
delete from works;

-- dependency level III

delete from job;
delete from offered_by;
delete from takes

-- dependency level II

delete from covers;
delete from has_skill;
delete from phone;
delete from pre_req;
delete from requires;
delete from section;

-- dependency level I

delete from course;
delete from person;
delete from position;
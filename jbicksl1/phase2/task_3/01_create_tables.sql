-- dependency level I

create table course
	(c_code			numeric(8,0),
	 title			varchar(80),
	 description		varchar(800),
	 status			varchar(7)
		check (status in ('active', 'expired')),
	 retail_price		numeric(8,2),
	 primary key (c_code)
	);

create table gics
	(ind_id			numeric(8,0),
	 title			varchar(80),
	 ind_level		numeric(1,0)
		check (ind_level <= 3 and ind_level >= 0),
	 description		varchar(800),
	 parent_ind_id		numeric(6,0),
	 primary key (ind_id),
	 check (case
			when ind_level = 0 then ind_id >= 10 and ind_id <= 99
			when ind_level = 1 then ind_id >= 1000 and ind_id <= 9999
			when ind_level = 2 then ind_id >= 100000 and ind_id <= 999999
			else ind_id >= 10000000 and ind_id <= 99999999
		end)
	);

create table person
	(person_id		numeric(8,0),
	 first_name		varchar(40),
	 last_name		varchar(40),
	 email			varchar(80), 
	 gender			varchar(6)
		check (gender in ('male', 'female', 'other')),
	 street_number		numeric(5,0),
	 street			varchar(40),
	 city			varchar(40),
	 state			varchar(40),
	 postal_code		numeric(8,0),
	 country		varchar(40),
	 primary key (person_id)
	);

create table position
	(pos_code		varchar(7),
	 title			varchar(80),
	 description		varchar(800),
	 pay_range_high		numeric(8,2),
	 pay_range_low		numeric(8,2),
	 pay_type		varchar(6)
		check (pay_type in ('wage', 'salary')),
	 primary key (pos_code),
	 check (not (pay_range_high < pay_range_low))
	);

create table skill
	(sk_code		numeric(8,0),
	 title			varchar(80),
	 primary key (sk_code)
	);

-- dependency level II

create table company
	(comp_id		numeric(8,0),
	 name			varchar(80),
	 website		varchar(80),
	 industry_group		numeric(8,0)
		check (industry_group >= 1000 and industry_group <= 9999),
	 street_number		numeric(5,0),
	 street			varchar(40),
	 city			varchar(40),
	 state			varchar(40),
	 postal_code		numeric(8,0),
	 country		varchar(40),
	 primary key (comp_id),
	 foreign key (industry_group) references gics (ind_id)
	);

create table covers
	(c_code			numeric(8,0),
	 sk_code		numeric(8,0),
	 sk_level		varchar(8)
		check (sk_level in ('beginner', 'medium', 'advanced')),
	 primary key (c_code, sk_code),
	 foreign key (c_code) references course (c_code),
	 foreign key (sk_code) references skill (sk_code)
	);

create table has_skill
	(person_id		numeric(8,0),
	 sk_code		numeric(8,0),
	 sk_level		varchar(8)
		check (sk_level in ('beginner', 'medium', 'advanced')),
	 primary key (person_id, sk_code),
	 foreign key (person_id) references person (person_id),
	 foreign key (sk_code) references skill (sk_code)
	);

create table phone
	(phone_number		numeric(10,0),
	 person_id		numeric(8,0),
	 primary key (phone_number),
	 foreign key (person_id) references person (person_id)
	);

create table pre_req
	(c_code			numeric(8,0),
	 prereq_c_code		numeric(8,0),
	 primary key (c_code, prereq_c_code),
	 foreign key (c_code) references course (c_code),
	 foreign key (prereq_c_code) references course (c_code)
	);

create table requires
	(pos_code		varchar(7),
	 sk_code		numeric(8,0),
	 sk_level		varchar(8)
		check (sk_level in ('beginner', 'medium', 'advanced')),
	 primary key (pos_code, sk_code),
	 foreign key (pos_code) references position (pos_code),
	 foreign key (sk_code) references skill (sk_code)
	);

create table section
	(c_code			numeric(8,0),
	 sec_no			numeric(8,0),
	 year			numeric(4,0),
	 complete_date		date,
	 format			varchar(16)
		check (format in ('classroom', 'online-sync', 'online-selfpaced', 'correspondence')),
	 price			numeric(8,2),
	 primary key (c_code, sec_no, year),
	 foreign key (c_code) references course (c_code)
	);

-- dependency level III

create table comp_sub_indus
	(comp_id		numeric(8,0),
	 ind_id			numeric(8,0),
	 primary key (comp_id, ind_id),
	 foreign key (comp_id) references company (comp_id),
	 foreign key (ind_id) references gics (ind_id)
	);

create table job
	(job_code		numeric(8,0),
	 pos_code		varchar(7),
	 emp_mode		varchar(9)
		check (emp_mode in ('full-time', 'part-time')),
	 pay_rate		numeric(8,2),
	 pay_type		varchar(6)
		check (pay_type in ('wage', 'salary')),
	 cate_code		numeric(8,0),
	 comp_id		numeric(8,0),
	 primary key (job_code),
	 foreign key (pos_code) references position (pos_code),
	 foreign key (comp_id) references company (comp_id)
	);

create table offered_by
	(c_code			numeric(8,0),
	 comp_id		numeric(8,0),
	 primary key (c_code, comp_id),
	 foreign key (c_code) references course (c_code),
	 foreign key (comp_id) references company (comp_id)
	);

create table takes
	(person_id		numeric(8,0),
	 c_code			numeric(8,0),
	 sec_no			numeric(8,0),
	 year			numeric(4,0),
	 complete_date		date,
	 primary key (person_id, c_code, sec_no, year),
	 foreign key (person_id) references person (person_id),
	 foreign key (c_code, sec_no, year) references section (c_code, sec_no, year),
	 check (not ((extract (year from complete_date)) < year))
	);

-- dependency level IV

create table req_by_job
	(job_code		numeric(8,0),
	 sk_code		numeric(8,0),
	 sk_level		varchar(8)
		check (sk_level in ('beginner', 'medium', 'advanced')),
	 primary key (job_code, sk_code),
	 foreign key (job_code) references job (job_code),
	 foreign key (sk_code) references skill (sk_code)
	);

create table works
	(person_id		numeric(8,0),
	 job_code		numeric(8,0),
	 start_date		date,
	 end_date		date,
	 primary key (person_id, job_code, start_date),
	 foreign key (person_id) references person (person_id),
	 foreign key (job_code) references job (job_code),
	 check (not (end_date < start_date))
	);
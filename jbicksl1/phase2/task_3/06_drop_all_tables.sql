-- dependency level IV

drop table req_by_job;
drop table works;

-- dependency level III

drop table comp_sub_indus;
drop table job;
drop table offered_by;
drop table takes;

-- dependency level II

drop table company;
drop table covers;
drop table has_skill;
drop table phone;
drop table pre_req;
drop table requires;
drop table section;

-- dependency level I

drop table course;
drop table gics;
drop table person;
drop table position;
drop table skill;